from flask import Flask, request
from flask_caching import Cache 
from datetime import datetime
import requests 

cache = Cache( )

app = Flask(__name__)

app.config['CACHE_TYPE'] = "simple"

cache.init_app( app )

API_URL = "http://api.openweathermap.org/data/2.5/weather"
APP_ID = "1508a9a4840a5574c822d70ca2132032"


@app.route( "/weather", methods=['GET'] )
@cache.cached( timeout=120 )
def get_weather( ):
  city = request.args.get( "city" )
  country = request.args.get( "country" )
  payload = {
    "appid": APP_ID,
    "q":f"{city.lower( )},{country.lower( )}"
  }
  r = requests.get( API_URL, params=payload ).json( )
  print ("Consultando")
  return { 
    "location_name":  f"{city.title( )}, {country.upper( )}", 
    "temperature" : f"{r['main']['temp'] - 273:.0f} ºC", # la temperatura estaba en kelvin 
    "wind": f"{r['weather'][0]['main']}, {r['wind']['speed']} m/s, {cardinal( r['wind']['deg'] )}",
    "cloudiness": f"{r['weather'][0]['description']}",
    "pressure": f"{r['main']['pressure']} hpa",
    "humidity": f"{r['main']['humidity']}%",
    "sunrise": datetime.fromtimestamp(r['sys']['sunrise']).strftime('%H:%M'),
    "sunset": datetime.fromtimestamp(r['sys']['sunset']).strftime('%H:%M'),
    "geo_coordinates": f"[{r['coord']['lat']}, {r['coord']['lon']}]",
    "requested_time": datetime.now( ).strftime('%Y-%m-%d %H:%M:%S'),
    "forecast": {}
    
  }

def cardinal( angle ):
  # el angulo esta expresado en grado sexagesimal, no es radian
  dirs = [
  'north', 
  'north-northeast', 
  'north-east', 
  'east-northeast', 
  'east', 
  'east-southeast', 
  'south-east', 
  'south-southeast', 
  'south', 
  'south-southwest', 
  'south-west', 
  'west-southwest', 
  'west', 
  'west-northwest', 
  'north-west', 
  'north-northwest'
  ]
  ix = round(angle / (360. / len(dirs)))
  return dirs[ix % len(dirs)]